<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\ViewComposers\NavigationComposer;

class ComposerServiceProvider extends ServiceProvider
{
    public function boot()
    {
        view()->composer('*', NavigationComposer::class);
    }

    public function register()
    {
        //
    }
}
