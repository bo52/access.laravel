<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;

class NavigationComposer
{
    public function compose(View $view)
    {
        return $view->with('mainitems', '43');
    }
}
