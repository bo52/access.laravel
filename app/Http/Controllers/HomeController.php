<?php

namespace App\Http\Controllers;

use App\Employee;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show($name)
    {
        $m='\App\\'.$name;
        $rows=$m::orderBy('created_at','asc')->get();
        return view('rows',[
            'rows'=>$rows,
            'name'=>$name,
            'arr'=>test('\home\\')
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return $this->show('Client');
        //return view('home');
    }

    public function user_new($name){
        if ($name!='User') return false;

        $n=date('Ymdhms');

        $m = '\App\\User';
        $row = new $m;
        $row->name = 'user'.$n;
        $row->email = $n.'@gmail.com';
        $row->password = Hash::make('123');
        $row->save();

        return true;
    }

    public function create(Request $request,$name)
    {
        if ($this->user_new($name))
            return redirect('/home/' . $name);

        $m = '\App\\' . $name;
        $row = new $m;
        $row->name = $name;
        $row->save();

        return redirect('/home/' . $name);
    }


    public function save(Request $request,$name,$id)
    {

        $m = '\App\\' . $name;
        $data = [];
        $data['name'] = $request->name;

        if ($name == 'User')
            return $this->user_save($m, $data, $name, $request, $id);

        if ($name == 'Client'){
            $data['gender'] = $request->gender;
            $data['email'] = $request->email;
            $data['phone'] = $request->phone;
            $data['comment'] = $request->comment;
        };

        if ($name == 'Role'){
            $data['contacts']='';
            $data['comments']='';
            for ($i = 0; $i < 5; $i++){
                $data['contacts'] = $data['contacts'].$request['contacts_select'.$i];
                $data['comments'] = $data['comments'].$request['comments_select'.$i];
            }
        };


        $m::where('id', $id)->update($data);

        return redirect('/home/'.$name.'/'.$id);
    }

    public function user_save($m,$data,$name,$request,$id){
        $row = $m::find($id);

        if ($row->name!=$request->name) {
            $validator = \Illuminate\Support\Facades\Validator::make($request->all(), ['role'=>'required','name' => 'required|max:255|unique:users']);

            if ($validator->fails()) {
                return redirect('/home/' . $name . '/' . $id)
                    ->withInput()
                    ->withErrors($validator);
            }
        }
        $data['role_id'] = $request->role;
        $group=$request->group;

        $m::where('id', $id)->update($data);

        $M='\App\\Employee';
        if ($group=='') {
            $rows = $M::orderBy('created_at', 'asc')->where('user_id',$id);
            if (count($rows)>0)
                $rows->delete();
        } else {
            $groups=explode(",", $group);
            $groups_old=explode(",", $request->group_old);

            foreach ($groups_old as $old) {
                if (in_array($old, $groups) == false) {
                    $rows = $M::orderBy('created_at', 'asc')->where('user_id', $id);
                    if (count($rows) > 0)
                        $rows->delete();
                }
            }

                foreach ($groups as $g) {
                    $int = (integer)($g);
                    $rows = $M::orderBy('created_at', 'asc')->where(['user_id' => $id, 'group_id' => (integer)$g])->get();
                    if (count($rows) == 0) {
                        $row = new $M;
                        $row->user_id = $id;
                        $row->group_id = $int;

                        $row->save();

                    }
                }
        }

        return redirect('/home/'.$name.'/'.$id);
    }


}
