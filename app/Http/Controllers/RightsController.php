<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NavigationComposer
{
    public function compose(View $view)
    {
        return $view->with('mainitems', '22');
    }
}

class RightsController extends Controller
{
    function show($id){
        $task=App\Task::find($id);
        return view('test.show',compact('task'));
    }
    function index1(){
        $tasks=App\Task::all();
        return view('test.index',compact('tasks'));
    }

    function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255'
        ]);
        if ($validator->fails()) {
            return redirect('/')
                ->withInput()
                ->withErrors($validator);
        }

        $ROW = new \App\Task;
        $ROW->name = $request->name;
        $ROW->save();

        return redirect('/');
    }

    public function index()
    {
        //$es = App\ИмяМодели::all();
        $tabs=['rouls','users','groups'];
        $title='система разграничения прав для (ролей, групп,пользователей)';
        return view('tabs.tabs', compact('tabs','title'));
    }
}
