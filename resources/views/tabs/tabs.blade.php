@extends('index')

@section('content')
<div class="tabs" class="tabs-header">
    <ul>
        @foreach($tabs as $tab)
            @component('tabs.header')
                @slot('index')
                    {{$loop->index}}
                @endslot
                @slot('tab')
                    {{$tab}}
                @endslot
            @endcomponent
        @endforeach

            @foreach($tabs as $tab)
                @component('tabs.item')
                    @slot('index')
                        {{$loop->index}}
                    @endslot
                    @slot('tab')
                        {{$tab}}
                    @endslot
                @endcomponent
            @endforeach
    </ul>
</div>

<script>
    $('.tabs').tabs();
</script>
@endsection

