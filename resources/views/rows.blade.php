
@extends('layouts.app')
@section('content')
    <div class="card-body">

        @include('errors')
        <form action="{{url('home/'.$name)}}" method="POST" class="form-horizontal">
            {{csrf_field()}}

            <div class="row">
                <div class="form-group">
                    <!--<label for="Task" class="col-sm-3 control-label">Имя*</label>-->
                    <div class="row">
                        <!--<div class="col-sm-6">
                        <input type="text" name="name" id="task-name" class="form-control">
                        </div>-->
                        <div class="col-sm-6">
                            <button type="submit" class="btn btn-success">
                                <i class="fa fa-plus"></i>
                                Добавить
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    @include('table',[$rows,$name])
@endsection
