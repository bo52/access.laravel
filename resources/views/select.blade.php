<select name="{{$type}}_select{{$index}}" class="selectpicker" data-show-subtext="true">
    @for ($i = 0; $i < 4; $i++)
            @switch($i)
            @case(0)
            @php ($a = 'red')
            @break
            @case(1)
            @php ($a = 'yellow')
            @break
            @case(2)
            @php ($a = 'blue')
            @break
            @case(3)
            @php ($a = 'green')
            @break
            @endswitch
        <option
            data-val="{{$i}}"
            name="{{$type}}_select{{$index}}"
            {{(strval($i)==substr($val,((int)(string)$index),1))?'selected':''}}
            value="{{$i}}"
            data-content="<i class='glyphicon {{$a}} glyphicon-stop'></i>">
        </option>
    @endfor
</select>
