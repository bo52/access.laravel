@if($name=='User')
    <label class="col-sm-3 control-label">Роль*</label>
    <div class="row">
        <div class="col-sm-6">
            <select name="role" class="selectpicker" data-show-subtext="true">
                <option value="empty"></option>
                @foreach($roles as $role)
                    <option
                        @if(strlen($row->role_id)>0)
                        @if($role->id==$row->role_id)
                         selected
                        @endif
                        @endif
                        value="{{$role->id}}" name="role">
                        {{$role->name}}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
    <label class="col-sm-3 control-label">Группы</label>
    <div class="row">
        <div class="col-sm-6">
            <input style="display: none" type="text" name="group_old" value="@foreach($employees as $e){{$e->group_id}},@endforeach">
            <input style="display: none" type="text" name="group" value="@foreach($employees as $e){{$e->group_id}},@endforeach">
            <select OnChange="test_group($(this))" multiple  class="select-group selectpicker" data-show-subtext="true">
                @foreach($groups as $group)
                    <option

                        @foreach($employees as $e)
                        @if($group->id==$e->group_id)
                        selected
                        @break
                        @endif
                        @endforeach

                        value="{{$group->id}}">
                        {{$group->name}}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
@endif
<script>

    function test_group(){
        var val='';
        $('.select-group').find('.selected').each(function(i,elem) {
            if (i!=0) val+=',';
            var index=$(elem).data('original-index');
            val+=$('.select-group').find('option:eq('+index+')').attr('value');
        });
        $('[name="group"]').attr('value',val);
    }
</script>

