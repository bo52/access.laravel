@if($name=='Role')
    <br>

    <div class="row">
    <label class="col-sm-3 control-label">Права</label>
    <div id="Access" class="col-sm-8">
        <table class="table table-border">
            <thead>
            <tr>
                <th class="danger">Запрещено</th>
                <th class="warning">Если ответственный</th>
                <th class="info">Для группы</th>
                <th class="success">Разрешено</th>
            </tr>
            </thead>
        </table>
    </div>
    </div>
    <div class="row">
        <label class="col-sm-3 control-label"></label>
        <div class="col-sm-8">
        <table class="access table table-bordered table-hover">

            <thead>
            <tr>
                <th>#</th>
                <th>Создание</th>
                <th>Просмотр</th>
                <th>Правка</th>
                <th>Удаление</th>
                <th>Экспорт</th>
            </tr>
            </thead>

            <tbody>
            <tr>
                <td>Контакты</td>
                    @for ($i = 0; $i < strlen ($row->contacts); $i++)
                        <td>
                            @component('select')
                                @slot('index')
                                    {{$i}}
                                @endslot
                                @slot('val')
                                {{$row->contacts}}
                                @endslot
                                    @slot('type')
                                        contacts
                                    @endslot
                            @endcomponent
                        </td>
                    @endfor
            </tr>
            <tr>
                <td>Комментарии</td>
                    @for ($i = 0; $i < strlen ($row->comments); $i++)
                        <td>
                            @component('select')
                                @slot('index')
                                    {{$i}}
                                @endslot
                                @slot('val')
                                    {{$row->comments}}
                                @endslot
                                    @slot('type')
                                        comments
                                    @endslot
                            @endcomponent
                        </td>
                    @endfor
            </tr>
            </tbody>
        </table>
    </div>
    </div>

@endif
