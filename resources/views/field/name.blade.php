@section('content')

    <div class="card">

        <div style="margin: 10px;" class="card-heading">
            <a href="{{$arr[$name][1].$name}}">{{$arr[$name][0]}}</a>
            \
            {{$row->id}}
            \
            {{$row->name}}
        </div>

        <form action="{{url('home/'.$name.'/'.($row->id).'/save')}}" method="POST" class="form-horizontal">
            {{csrf_field()}}

            <label for="Task" class="col-sm-3 control-label">Имя*</label>
            <div class="row">
                <div class="col-sm-6">
                    <input value="{{$row->name}}" type="text" name="name" id="row-name" class="form-control @error('name') is-invalid @enderror" required autocomplete="name">

                    @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror

                </div>
            </div>

            @include('field.role',[$name,$roles,$groups,$employees])

            @include('field.access',[$name,$row])
            @include('field.client',[$name,$row])
            @include('action',[$name,$row])
        </form>
        @include('forms.row_delete',[$name,$row])
    </div>
    <br>
    </div>
@endsection
