@if($name=='Client')
<label for="Task" class="col-sm-3 control-label">Телефон</label>
<div class="row">
    <div class="col-sm-6">
        <input value="{{$row->phone}}" type="text" name="phone" id="row-phone" class="form-control">
    </div>
</div>

<label for="Task" class="col-sm-3 control-label">e-mail</label>
<div class="row">
    <div class="col-sm-6">
        <input value="{{$row->email}}" type="text" name="email" id="row-email" class="form-control">
    </div>
</div>

<label for="Task" class="col-sm-3 control-label">Пол</label>
<div class="row">
    <div class="col-sm-3">
        <input type="radio" name="gender" value="0" id="row-gender-0">
        <label for="row-gender-0" class="control-label">Муржской</label>
    </div>
    <div class="col-sm-3">
        <input type="radio" name="gender" value="1" id="row-gender-1">
        <label for="row-gender-1" class="control-label">Женский</label>
    </div>
</div>

<label for="Task" class="col-sm-3 control-label">Комментарий</label>
<div class="row">
    <div class="col-sm-6">
        <textarea type="text" name="comment" id="row-comment" class="form-control">{{$row->comment}}</textarea>
    </div>
</div>
<script>
    $('#row-gender-{{$row->gender}}').attr('checked',true);
</script>
@endif
