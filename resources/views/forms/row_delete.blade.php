@section('row-delete')

    <div class="col-sm-2">
    <form action="{{url('home/'.$name.'/'.($row->id))}}" method="POST">
        {{csrf_field()}}
        {{method_field('DELETE')}}
        <button class="btn btn-danger">
        Delete
        </button>
    </form>

    </div>
@show
