
@section('menu')
    <div class="container-fluid">
        <!-- Основная часть меню (может содержать ссылки, формы и другие элементы) -->
        <div class="collapse navbar-collapse" id="navbar-main">
            <!-- Содержимое основной части -->

            <ul class="nav navbar-nav">
                @foreach ($arr as $key => $value)
                    <li><a name="menu-{{$key}}" href="{{$value[1].$key}}">{{$value[0]}}</a></li>
                @endforeach
            </ul>

        </div>
    </div>
<script>
    $('[name="menu-{{$name}}"]').parent().addClass('active');
</script>
@show


