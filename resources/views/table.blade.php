@if(count($rows)>0)
    <div class="card">
        <!--<div class="card-heading">Current tasks</div>-->
        <div class="card-body">
            <table class="table table-striped task-table">
                <!--<thead><th>Task</th><th>&nbsp</th></thead>-->
                <tbody>
                @foreach ($rows as $row)
                    @if ($row->name=='root')
                        @continue
                    @endif
                    <tr>
                        <td class="table-text">
                            <a href="{{$name}}/{{$row->id}}">{{$row->name}}</a>
                        </td>
                        <td>
                            @include('forms.row_delete',[$name,$row])
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endif
