<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Route::get('/tabs','RightsController@index');


use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

function test($path){
    $arr=[
        'Role'=>['Роли',$path],
        'User'=>['Пользователи',$path],
        'Group'=>['Группы',$path],
        'Client'=>['Клиенты',$path]
    ];
    //foreach ($arr as $e){
   //}
    return $arr;
}

Route::delete('/home/{name}/{id}',function($name,$id){
    $m='\App\\'.$name;
    $row = $m::find($id);
    $row->delete();

    return redirect('/home/'.$name);
});

Route::get('/home/{name}/{id}',function($name,$id){
    $m='\App\\'.$name;
    $row=$m::find($id);


    $roles=[];
    $groups=[];
    $employees=[];
    if ($name=='User'){
        $m='\App\\Role';
        $roles=$m::orderBy('created_at','asc')->get();

        $m='\App\\Employee';
        $employees=$m::orderBy('created_at','asc')->where('user_id', $id)->get();

        $m='\App\\Group';
        $groups=$m::orderBy('created_at','asc')->get();
    }

    return view('row',[
        'row'=>$row,
        'name'=>$name,
        'id'=>$id,
        'roles'=>$roles,
        'groups'=>$groups,
        'employees'=>$employees,
        'arr'=>test('..\\')
    ]);
});

Route::get('/', function () {
    return view('auth.login');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/home/{name}','HomeController@show');
Route::post('/home/{name}','HomeController@create');
Route::post('/home/{name}/{id}/save','HomeController@save');
